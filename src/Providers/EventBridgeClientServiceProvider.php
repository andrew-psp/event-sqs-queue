<?php

namespace PSPMedia\EventSqsQueue\Providers;

use Aws\EventBridge\EventBridgeClient;
use Illuminate\Support\ServiceProvider;

class EventBridgeClientServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(EventBridgeClient::class, function () {
            $config = config('services.aws');
            return new EventBridgeClient([
                'version' => 'latest',
                'region' => $config['region']
            ]);
        });
    }
}
