<?php

namespace PSPMedia\EventSqsQueue\Queue;

use Illuminate\Queue\SqsQueue;
use PSPMedia\EventSqsQueue\Queue\Jobs\EventSqsQueueJob;

class EventSqsQueue extends SqsQueue
{
    public function pop($queue = null)
    {
        $response = $this->sqs->receiveMessage([
            'QueueUrl' => $queue = $this->getQueue($queue),
            'AttributeNames' => ['ApproximateReceiveCount'],
        ]);

        if (! is_null($response['Messages']) && count($response['Messages']) > 0) {
            return new EventSqsQueueJob(
                $this->container, $this->sqs, $response['Messages'][0],
                $this->connectionName, $queue
            );
        }
    }
}
