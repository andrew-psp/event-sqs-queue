<?php

namespace PSPMedia\EventSqsQueue\Notifications\Channels;

use Aws\EventBridge\EventBridgeClient;
use Illuminate\Support\Arr;
use PSPMedia\EventSqsQueue\Notifications\EventBridgeBaseNotification;

class EventBridgeChannel
{
    private EventBridgeClient $client;

    public function __construct(EventBridgeClient $client)
    {
        $this->client = $client;
    }

    public function send($notifiable, EventBridgeBaseNotification $notification)
    {
        $detail = [
            'event'   => $notification->eventName(),
            'payload' => $notification->payload()
        ];

        $entry = [
            'Detail'     => json_encode($detail),
            'DetailType' => 'event',
            'Resources'  => $notification->resources(),
            'Source'     => config('event-sqs-queue.event-source'),
            'Time'       => now()->toAtomString(),
        ];

        $response = $this->client->putEvents([
            'Entries' => [$entry]
        ]);

        foreach ($response->get("Entries") as $responseEntry) {
            throw_if(Arr::get($responseEntry, 'ErrorMessage'),
                new \Exception('EventBridge message was not sent. ' . json_encode($response->get("Entries"))));
        }

        throw_if(Arr::get($response->get('@metadata'), 'statusCode') != 200,
            new \Exception('EventBridge message was not sent. ' . json_encode($entry) ));
    }
}
