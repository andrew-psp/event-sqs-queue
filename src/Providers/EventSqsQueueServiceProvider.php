<?php

namespace PSPMedia\EventSqsQueue\Providers;

use PSPMedia\EventSqsQueue\Queue\Connectors\EventSqsQueueConnector;
use Illuminate\Support\ServiceProvider;

class EventSqsQueueServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['queue']->extend('event-sqs-queue', function () {
            return new EventSqsQueueConnector;
        });
    }
}
