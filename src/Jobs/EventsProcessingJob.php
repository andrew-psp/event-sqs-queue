<?php

namespace PSPMedia\EventSqsQueue\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;
use PSPMedia\EventSqsQueue\Exceptions\EmptyEventNameException;

class EventsProcessingJob implements ShouldQueue
{
    private ?array $payload;

    private ?string $eventName;

    public function __construct(string $eventName = null, array $payload = null)
    {
        $this->eventName = $eventName;
        $this->payload = $payload;
    }

    public function handle()
    {
        throw_unless($this->eventName, new EmptyEventNameException('EventName is required parameter. ' . json_encode((array)$this->payload)));

        if(($listeners = $this->getListeners()) === null) {
            return;
        }

        foreach (Arr::wrap($listeners) as $listener) {
            dispatch(new $listener($this->payload));
        }
    }

    private function getListeners()
    {
        return Arr::get(config('event-sqs-queue.listen'), $this->eventName);
    }
}