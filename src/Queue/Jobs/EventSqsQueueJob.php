<?php

namespace PSPMedia\EventSqsQueue\Queue\Jobs;

use Illuminate\Queue\Jobs\SqsJob;
use Illuminate\Queue\CallQueuedHandler;
use Illuminate\Support\Arr;
use PSPMedia\EventSqsQueue\Jobs\EventsProcessingJob;

class EventSqsQueueJob extends SqsJob
{
    public function payload()
    {
        $commandName = EventsProcessingJob::class;
        $command = $this->makeCommand($commandName, json_decode($this->job['Body'], true));

        return [
            'displayName' => $commandName,
            'job' => CallQueuedHandler::class . '@call',
            'data' => compact('commandName', 'command'),
        ];
    }

    protected function makeCommand($commandName, $body): string
    {
        $properties = ['eventName' => Arr::get($body, 'event'),
                       'payload' => Arr::get($body, 'payload')];
        return serialize($this->container->make($commandName, $properties));
    }
}
