<?php

namespace PSPMedia\EventSqsQueue\Notifications;

use Illuminate\Notifications\Notification;
use PSPMedia\EventSqsQueue\Notifications\Channels\EventBridgeChannel;

abstract class EventBridgeBaseNotification extends Notification
{
    public function via($notifiable): array
    {
        return [EventBridgeChannel::class];
    }

    abstract public function eventName();

    abstract public function payload();

    abstract public function resources();
}
