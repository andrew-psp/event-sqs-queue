<?php

namespace PSPMedia\EventSqsQueue\Exceptions;

use Exception;

class EmptyEventNameException extends Exception
{
    protected $message = 'EventName is required parameter';
}
