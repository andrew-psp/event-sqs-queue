<?php

return [
    'connections' => [
        'event-sqs-queue' => [
            'driver' => 'event-sqs-queue',
            'key'    => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'prefix' => env('SQS_PREFIX'),
            'queue'  => env('AWS_EVENT_SQS_QUEUE', 'sqs queue name'),
            'region' => env('AWS_REGION'),
        ],
    ],
];
